# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

'''
Modulo BASE

@author Stefan Yohansson da Silva Areeira Pinto
@version 0.1

View base é o pai de todas as views do sistema com os metodos de criar, listar, editar e excluir
'''

#@TODO: fazer a importação de cada modulo que for usar dinamicamente
#@TODO: fazer outros metodos e testar com o modulo de client

dic = {}

def create(request, formName, module, dic, template = 'basic/create.html', formData = None):
    forms = getattr(__import__(module, fromlist=['forms']), 'forms')
    methodtoCall = getattr(forms, formName)
    if request.POST:
        form = methodtoCall(request.POST)

        if form.is_valid():
            inserted = form.save()
            if request.POST['redirect']:
                return HttpResponseRedirect(reverse(request.POST['redirect'], args=[inserted.pk,]))
            else:
                # Redirecionar para view para ver os dados inseridos
                pass
                #return HttpResponseRedirect(reverse(request.POST['redirect'], args=[inserted.pk,]))
            # Adicionar redirecionamento vindo de id escondida no create

    else:
        if formData:
            form = methodtoCall(formData)
        else:
            form = methodtoCall()
        
    dic.update({
        'form' : form
        })
    
    return render_to_response(template, dic, context_instance=RequestContext(request))
