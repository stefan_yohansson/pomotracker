from django.db import models

class Pomodoro(models.Model):
    task = models.ForeignKey('Task')
    pomodoro_number = models.IntegerField()
    hour_start = models.TimeField()
    hour_end = models.TimeField()

