#import models
from django import forms
from django.contrib.auth.models import User
from models import Task
from django.forms import ModelForm, TextInput, PasswordInput, Select, ChoiceField

class taskForm(ModelForm):
    class Meta:
        model = Task
        
        fields = ('name', 'description', 'estimated_pomodoro')
        
        widgets = {
            #'user' : TextInput()
        }

