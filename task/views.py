from basic import views as baseView
from django.contrib.auth.models import User


dic = {}

def create(request):
    dic.update({
        'title_page' : 'Add a Task',
        })
    
    formName = 'taskForm'
    module = 'task'
    template = 'basic/create.html'
    
    return baseView.create(request, formName, module, dic, template)
