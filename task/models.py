from django.db import models

class Task(models.Model):
    name = models.CharField(max_length=45)
    description = models.TextField()
    estimated_pomodoro = models.IntegerField()
    real_pomodoro = models.IntegerField(blank=True, null=True)
    date_start = models.DateTimeField(auto_now_add=True)
    date_end = models.DateTimeField(blank=True, null=True)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.name
